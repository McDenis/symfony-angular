<?php
namespace App\Controller;

use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class HomeController extends AbstractController {
    /**
    *  @var Environment
    */

    public function __construct(Environment $twig){
        $this->twig = $twig;
    }

    /**
     * @Route("/", name="home")
     * @param PropertyRepository $repository
     * @return Response
     */
    public function index(PropertyRepository $repository): Response
    {
        $properties = $repository->findLatest();
        return $this->render('pages/home.html.twig', [
            'properties' => $properties,
            'user' => $this->getUser()
        ]);
    }

    /* Ping les autres utilisateurs sur la page tchat (mercure) */
    /**
     * @Route("/tchat", name="tchat", methods="post")

    public function ping(Publisher $publisher){
        $update = new Update("http://localhost:8000/tchat", "[]");
        $publisher($update);
        return $this->redirectToRoute("tchat");
    } */
}
