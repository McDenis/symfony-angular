<?php

namespace App\DataFixtures;

use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    //Permet de créer le premier utilisateur (sans le rentrer vulgairement dans la bdd)
    public function load(ObjectManager $manager)
    {
       $user = new Users();
       $user->setUsername('demo');
       $user->setPassword($this->encoder->encodePassword($user, 'demo'));
       $manager->persist($user);

        $manager->flush();
    }
}
